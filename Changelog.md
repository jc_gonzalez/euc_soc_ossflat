Change Log
====================

All notable changes to the Euclid SOC QLA Report Flattener software project will be 
documented in this file.

V1.0 / 2020-05-12
--------------------------

First release of the Euclid SOC QLA Report Flattener.

