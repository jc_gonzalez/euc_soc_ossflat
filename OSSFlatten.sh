#!/bin/bash 
#------------------------------------------------------------------------------------
# OSSFlatten.sh
# ESS OSS Flattener (converts to CSV for import into ARES)
#
# Created by J C Gonzalez <jcgonzalez@sciops.esa.int>
# Copyright (C) 2015-2020 by Euclid SOC Team
#------------------------------------------------------------------------------------
SCRIPTPATH=$(cd $(dirname $(which $0)); pwd; cd - > /dev/null)
export PYTHONPATH=${SCRIPTPATH}

echo ""
${PYTHON:=python3} ${SCRIPTPATH}/oss_flatten/oss_flatten.py $*
