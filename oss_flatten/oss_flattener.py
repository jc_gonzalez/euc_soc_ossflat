#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Defines a class to parse an OSS and return a flattened version (CSV) of it.
"""
#----------------------------------------------------------------------

import os, sys

_filedir_ = os.path.dirname(os.path.realpath(__file__))
_appsdir_, _ = os.path.split(_filedir_)
_basedir_, _ = os.path.split(_appsdir_)
sys.path.insert(0, os.path.abspath(os.path.join(_filedir_, _basedir_, _appsdir_)))

PYTHON2 = False
PY_NAME = "python3"

#----------------------------------------------------------------------

import logging
logger = logging.getLogger()

from pprint import pprint

import xml.etree.ElementTree as ET

#----------------------------------------------------------------------

VERSION = '0.0.1'

__author__     = "J. C. Gonzalez"
__version__    = VERSION
__license__    = "LGPL 3.0"
__status__     = "Development"
__copyright__  = "Copyright (C) 2015-2020 by Euclid SOC Team @ ESAC / ESA"
__email__      = "jcgonzalez@sciops.esa.int"
__date__       = "2020-02-03"
__maintainer__ = "Euclid SOC Team"
#__url__       = ""

#----------------------------------------------------------------------

class OSSFlattener:
    """
    Class to process an input OSS and generate the list of pointings and related information in the
    form of a CSV file, to be imported by the HMS subsystem.
    """
    ObsTranslationTableAttr = { "id": "obsId", "obsRequestId": "obsRqstId",
                                "patchId": "obsPatchId", "fieldId": "obsFieldId" }
    ObsTranslationTable = { "Target": "obsTgt",
                            "ObservationType": "obsType",
                            "SurveyId": "obsSurvId",
                            "MissionPhase": "obsMissionPhase",
                            "NPointings": "obsNPts",
                            "Priority": "obsPriority" }
    PtTranslationTableAttr = { "id": "pointingId", "inObsId": "inObsId", "planningId": "planId" }
    PtTranslationTable = { "SlewType": "slwType",
                            "SlewMargin": "slwMargin",
                            "SlewDuration": "slwDur",
                            "SAA": "saa",
                            "PointingActivity": "pointAct",
                            "PlanningStatus": "planStatus",
                            "Mjd2000": "mjd2000",
                            "EigenSlewSize": "eigSlwSize",
                            "EarliestSlewStartTime": "earlSlwStart",
                            "Duration": "duration",
                            "Alpha": "alpha",
                            "EclipticAttitude/PositionAngle": "attEclipPosAngle",
                            "EclipticAttitude/Longitude": "attEclipLon",
                            "EclipticAttitude/Latitude": "attEclipLat",
                            "EclipticAttitude/Frame": "attEclipFrame",
                            "EquatorialAttitude/PositionAngle": "attEqPosAngle",
                            "EquatorialAttitude/Longitude": "attEqLon",
                            "EquatorialAttitude/Latitude": "attEqLat",
                            "EquatorialAttitude/Frame": "attEqFrame",
                            "AttitudeQuaternion/Qw": "attQw",
                            "AttitudeQuaternion/Qx": "attQx",
                            "AttitudeQuaternion/Qy": "attQy",
                            "AttitudeQuaternion/Qz": "attQz" }
    FoVTranslationTableAttr = { "long1": "fovLon1", "lat1": "fovLat1", "long2": "fovLon2", "lat2": "fovLat2",
                                "long3": "fovLon3", "lat3": "fovLat3", "long4": "fovLon4", "lat4": "fovLat4" }

    Definitions = { "obsId": ("STRING", "Observation Id.", ""),
                    "obsRqstId": ("STRING", "Observation Request Id.", ""),
                    "obsPatchId": ("STRING", "Observation Patch Id.", ""),
                    "obsFieldId": ("STRING", "Observation Field Id.", ""),
                    "obsTgt": ("STRING", "Observation Target", ""),
                    "obsType": ("STRING", "Observation Type", ""),
                    "obsSurvId": ("STRING", "Survery Id.", ""),
                    "obsMissionPhase": ("STRING", "Mission Phase", ""),
                    "obsNPts": ("INTEGER", "Number of Obs. Pointings", ""),
                    "obsPriority": ("STRING", "Scientific Obs. Priority", ""),
                    "pointingId": ("STRING", "Pointing Id.", ""),
                    "inObsId": ("STRING", "Pointing number within observation", ""),
                    "planId": ("STRING", "Planning Id.", ""),
                    "slwType": ("STRING", "Slew Type", ""),
                    "slwMargin": ("INTEGER", "Slew Margin", ""),
                    "slwDur": ("INTEGER", "Slew Duration", ""),
                    "saa": ("DOUBLE", "Solar Aspect Angle", "deg"),
                    "pointAct": ("STRING", "Pointing Activity", ""),
                    "planStatus": ("STRING", "Planning Status", ""),
                    "mjd2000": ("INTEGER", "MJD2000", ""),
                    "eigSlwSize": ("DOUBLE", "Eigen Slew Size", ""),
                    "earlSlwStart": ("INTEGER", "Easliest Slew Start Time", ""),
                    "duration": ("INTEGER", "Duration", "s"),
                    "alpha": ("DOUBLE", "Alpha", "deg"),
                    "attEclipPosAngle": ("DOUBLE", "Attitude Ecliptic Position Angle", "deg"),
                    "attEclipLon": ("DOUBLE", "Attitude Ecliptic Longitude", "deg"),
                    "attEclipLat": ("DOUBLE", "Attitude Ecliptic Latitude", "deg"),
                    "attEclipFrame": ("STRING", "Attitude Ecliptic Frame", ""),
                    "attEqPosAngle": ("DOUBLE", "Attitude Equatorial Position Angle", "deg"),
                    "attEqLon": ("DOUBLE", "Attitude Equatorial Longitude", "deg"),
                    "attEqLat": ("DOUBLE", "Attitude Equatorial Latitude", "deg"),
                    "attEqFrame": ("STRING", "Attitude Equatorial Frame", ""),
                    "attQw": ("DOUBLE", "Attitude Quaternion W Component", ""),
                    "attQx": ("DOUBLE", "Attitude Quaternion X Component", ""),
                    "attQy": ("DOUBLE", "Attitude Quaternion Y Component", ""),
                    "attQz": ("DOUBLE", "Attitude Quaternion Z Component", ""),
                    "fovLon1": ("DOUBLE", "FoV 1st Point Longitude", "deg"),
                    "fovLat1": ("DOUBLE", "FoV 1st Point Latitude", "deg"),
                    "fovLon2": ("DOUBLE", "FoV 2nd Point Longitude", "deg"),
                    "fovLat2": ("DOUBLE", "FoV 2nd Point Latitude", "deg"),
                    "fovLon3": ("DOUBLE", "FoV 3rd Point Longitude", "deg"),
                    "fovLat3": ("DOUBLE", "FoV 3rd Point Latitude", "deg"),
                    "fovLon4": ("DOUBLE", "FoV 4th Point Longitude", "deg"),
                    "fovLat4": ("DOUBLE", "FoV 4th Point Latitude", "deg") }

    Prefixes = ['OSS_']

    def __init__(self, file):
        """
        Initialize class instance
        """
        self.file = file
        try:
            logger.info(f'Loading input OSS file {file} . . .')
            with open(file, 'r') as fxml:
                xmlString = fxml.read()
            strippedXmlString = ''.join(line[:-1].lstrip(' \t') for line in xmlString.splitlines(True))
            del xmlString
            self.root = ET.fromstring(strippedXmlString)
            #pprint(strippedXmlString)
            del strippedXmlString
        except Exception as ee:
            logger.error(f"Cannot open file {file}: {str(ee)}")

    def processObs(self, obs):
        """
        Get the data for an observation
        """
        # Take obs. attributes
        trTbl = OSSFlattener.ObsTranslationTableAttr
        self.obs = [(attrTr, obs.attrib[attr]) for attr, attrTr in trTbl.items()]
        # Take obs. children (no Pointing)
        trTbl = OSSFlattener.ObsTranslationTable
        for tag, tagTr in trTbl.items():
            node = obs.find(tag)
            if node is not None:
                self.obs.append((tagTr, node.text))

        # Process pointings in the observation
        for pointing in obs.findall('Pointing'):
            # Get date for pointing
            thePtDate = pointing.find('StartTimeUtc').text

            # Generate obs. information for that date
            for tag, value in self.obs:
                self.csv.append( (tag, thePtDate, value) )

            # Generate specific pointing data
            # Take pt. attributes
            trTbl = OSSFlattener.PtTranslationTableAttr
            ptInfo = [(attrTr, pointing.attrib[attr]) for attr, attrTr in trTbl.items()]
            # Take pt. children
            trTbl = OSSFlattener.PtTranslationTable
            ptInfo.extend([(tagTr, pointing.find(tag).text) for tag, tagTr in trTbl.items()])
            # Take FoV info
            trTbl = OSSFlattener.FoVTranslationTableAttr
            fov = pointing.find('FoV')
            ptInfo.extend([(attrTr, fov.attrib[attr]) for attr, attrTr in trTbl.items()])

            # Generate pt. information
            for tag, value in ptInfo:
                self.csv.append((tag, thePtDate, value))

    def processOSS(self):
        """
        Process entire OSS, obs. by obs.
        """
        csvfile = '.'.join([self.file, 'csv'])
        deffile = '.'.join([self.file, 'def.csv'])

        # Generate pointing output CSV file
        nobs = 0
        nentries = 0
        nentriesgrp = 0
        ngrpprev = 0
        with open(csvfile, 'w') as fcsv:
            fcsv.write('Parameter, Time, Value\n')
            for obs in self.root.findall('Observation'):
                self.csv = []
                self.processObs(obs)
                nobs = nobs + 1
                nentries = nentries + len(self.csv)
                ngrp, rem = divmod(nentries, 1000000)
                if ngrp > ngrpprev:
                    ngrpprev = ngrp
                    logger.info(f'Processed {nobs} observations, {nentries} entries generated . . .')
                for prefix in OSSFlattener.Prefixes:
                    for tag, timestamp, value in self.csv:
                        fcsv.write(f'{prefix}{tag}, {timestamp}, {value}\n')

        logger.info(f'Processed {nobs} observations, {nentries} entries generated.')

        # Generate pointing output CSV file
        logger.info('Generating definition file . . .')
        with open(deffile, 'w') as fdef:
            fdef.write('Parameter, Type, Description, Unit\n')
            for prefix in OSSFlattener.Prefixes:
                for tag, values in OSSFlattener.Definitions.items():
                    ttype, tdef, tunit = [f'"{v}"' for v in values]
                    fdef.write(f'"{prefix}{tag}", {ttype}, {tdef}, {tunit}\n')


    def run(self):
        """
        Process selected file
        """
        self.processOSS()

if __name__ == '__main__':
    pass
